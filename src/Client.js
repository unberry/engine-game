const PropsManager = require('./PropsManager');
const Bootloader = require('./Bootloader');
const LifecycleManager = require('./LifecycleManager');
/**
 * Client Main Class for running the application
 * */


exports = module.exports = class Client {


    static get GgcBootloader() {
        if (!Client._bootloaderIn) Client._bootloaderIn = new Bootloader(Client.GgcPropsManager, Client);
        return Client._bootloaderIn;
    }

    static get GgcLifeCycleManager() {
        if (!Client._lifecycleManagerIn) Client._lifecycleManagerIn = new LifecycleManager(Client.GgcPropsManager, Client);
        return Client._lifecycleManagerIn;
    }

    static get GgcPropsManager() {
        return PropsManager;
    }

};
