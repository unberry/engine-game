/**
 * Class Bootloader
 * */

exports = module.exports = class Bootloader {

    constructor(propsManager, ctx) {
        this.props = propsManager;
        this.ctx = ctx;
    }

    appWillLoad(loaderFunction) {
        if (this.props.doLog) console.log('Setting Loader Function', loaderFunction);
        this._loaderFn = loaderFunction;
    }

    appWillUnload(unloaderFunction) {
        if (this.props.doLog) console.log('Setting UN-Loader Function', unloaderFunction);
        this._unloaderFn = unloaderFunction;
    }

    startApp(element) {
        this._loaderFn(element);
    }

    async stoptApp() {
        if (this._unloaderFn) {
            await this._unloaderFn();
        }
    }

};
