/**
 * Class Props manager to save props
 * */

const props = {};

exports = module.exports = class PropsManager {
    static set rootElement(sVal) {
        if (PropsManager.doLog) console.log('Setting Root element as:', sVal);
        props.__rootElement = sVal;
    }

    static get rootElement() {
        return props.__rootElement;
    }

    static set doLog(bVal) {
        if (PropsManager.doLog) console.log('Setting doLog as:', bVal);
        props.__doLog = bVal;
    }

    static get doLog() {
        return props.__doLog;
    }
};
