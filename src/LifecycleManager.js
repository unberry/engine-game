const queryString = require('query-string');
const GgcServer = require('./GgcServer');

/**
 * Class Lifecycle Manager
 * */

exports = module.exports = class LifecycleManager {

    constructor(propsManager, ctx) {
        this.props = propsManager;
        this.ctx = ctx;
    }

    detectMode() {
        // looks for GgcServer to enable embed mode
        console.log(location.search);
        const parsed = queryString.parse(location.search);
        console.log(parsed);
        if (parsed._em === 'ggcs' && parsed._ggci && parsed.con) {
            this._ggci = parsed._ggci;
            this.con = parsed.con;
            this.ggcServer = new GgcServer(this._ggci, this.con);
            this._embedMode = true;
        } else {
            this._embedMode = !!(this.ggcServer && this._ggci && this.con);
        }
    }

    async activate() {
        this.detectMode();
        if (this.props.doLog) console.log('Starting the lifecycle');
        if (this._embedMode) {
            await this.execEmbeddedCycle();
        } else {
            await this.execRootCycle();
        }
    }

    async execRootCycle() {
        if (this.props.doLog) console.log('Launched on element', this.props.rootElement);
        this.ctx._bootloaderIn.startApp(this.props.rootElement);
    }

    async execEmbeddedCycle() {
        if (this.props.doLog) console.log('Executing embed cycle', this.props.rootElement);
        this._uuid = await this.ggcServer.register(this);
        this.sendLogEvent('Ready to load the game.');
        this.ggcServer.listen(this._uuid, 'START', (element) => {
            if(element && element.length) this.props.rootElement = element;
            if (this.props.doLog) console.log('Launched on element', this.props.rootElement);
            this.ctx._bootloaderIn.startApp(this.props.rootElement);
        });
        this.ggcServer.listen(this._uuid, 'FORCE_END', async () => {
            if (this.props.doLog) console.log('Force Ending The App');
            await this.ctx._bootloaderIn.stoptApp();
        });
    }

    sendStatEvent(prop, value) {
        try {
            this.ggcServer.dispatchEvent(this._uuid, 'STATS', {prop, value});
        } catch (c) {
            console.log(c);
        }
    }

    sendLogEvent(logMessage) {
        try {
            this.ggcServer.dispatchEvent(this._uuid, 'LOG', logMessage);
        } catch (c) {
            console.log(c);
        }
    }

    sendEndEvent() {
        try {
            this.ggcServer.dispatchEvent(this._uuid, 'END');
        } catch (c) {
            console.log(c);
        }
    }

    getSession() {
        return this.ggcServer ? this.ggcServer.getSession() : null;
    }

};
