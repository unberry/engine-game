const io = require("socket.io-client");

/**
 * GGc Server interface
 * */

class GgcServer {
    constructor(uuid, con) {
        this.uuid = uuid;
        this.con = con;
        this.socket = io.connect(this.con);
        this.socket.emit('handshake', this.uuid);
        this.socket.on('shakeback', data => {
            console.log('Shakeback Recieved!!', data);
        });
        this.handlers = {};
        this.session = {};
        this.handlers['SESSION'] = (data) => this.session = data;
        this.socket.on('comm_in', ({event, data}) => {
            console.log('Comm IN Recieved!!', event, data);
            if (this.handlers[event]) this.handlers[event](data);
            else console.log('No handler for the event.');
        });
    }

    async register(ctx) {
        this.socket.emit('comm', {event: 'REGISTER'});
        return this.uuid;
    }

    listen(uuid, event, handler) {
        this.handlers[event] = handler; //START, FORCE_END, SESSION send these 3 events
    }

    dispatchEvent(uuid, event, data) {
        this.socket.emit('comm', {event, data}); // Dispatch these events: STATS, LOG, END
    }

    getSession(){
        return this.session;
    }
}

exports = module.exports = GgcServer;
