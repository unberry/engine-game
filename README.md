# Grappus assessment Center Client

This is a module which is needed to convert any react (or vue or angular) application to a client of assessment center where the center can run it as embedded module.

# install in the apps
To install this client in ypour game / app just include this as dependency. **Make sure you have access to this git repo to install it**. Otherwise NPM install will hang until timeout or throw access error.

Add under `dependencies` in package.json
```json
"grappus-assessment-center-client": "git+ssh://git@gitlab.com:grappus-unberry/web/assessment-center-client.git"
```

## Example of React App

Here is the sample index file for react app demonstrating how it looks like.

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {GgcBootloader, GgcLifeCycleManager, GgcPropsManager} from 'grappus-assessment-center-client';


// Set The root element the app is rendered to
GgcPropsManager.doLog = true;
GgcPropsManager.rootElement = 'root';


GgcBootloader.appWillLoad(rootElement => {
    ReactDOM.render(
        <React.StrictMode>
            <App/>
        </React.StrictMode>,
        document.getElementById(rootElement)
    );
    // If you want your app to work offline and load faster, you can change
    // unregister() to register() below. Note this comes with some pitfalls.
    // Learn more about service workers: https://bit.ly/CRA-PWA
    serviceWorker.unregister();

});

GgcBootloader.appWillUnload(async () => {
    // called when app is shutting down.
    console.log('Shutting Down...');
});

// Activate the lifecycle execution
GgcLifeCycleManager.activate();


/**
 * Below is not needed,, just for demo purposes of some features.
 * 
 * You can import GgcLifeCycleManager anywhere and call these functions. Ex: fro many component
 * */
// JUST FOR DEMO, if you are running in game center, you can get the session data from here. Session will consist of data we need to pass in games, ex: user details, stats of previous games, etc.
console.log(GgcLifeCycleManager.getSession());
// If you need to tell the manager that game is ended
GgcLifeCycleManager.sendEndEvent();
// If you need to save any stats, ex any score. These will be available as an object for the game in the game center
GgcLifeCycleManager.sendStatEvent('score', 43);
GgcLifeCycleManager.sendStatEvent('timeTaken', 3212);
GgcLifeCycleManager.sendStatEvent('name entered in form', 'Any Text not just numbers');
GgcLifeCycleManager.sendStatEvent('didCheat', false);
// If you need to just log any message, and want it to be enabled in gama center result or report etc.
GgcLifeCycleManager.sendLogEvent('User changed tab');
GgcLifeCycleManager.sendLogEvent('User pasted the content: ...');
GgcLifeCycleManager.sendLogEvent('Level 4 loaded');
GgcLifeCycleManager.sendLogEvent('any message');

```

### Steps to convert
1. Import the 3 classes. `import {GgcBootloader, GgcLifeCycleManager, GgcPropsManager} from 'grappus-assessment-center-client';`
2. Put your launcher code in `GgcBootloader.appWillLoad` method. Make sure the elment given in fn param is used. This is for react, we can use similar for others.
3. Can optionally call graceful shutdown handler: `GgcBootloader.appWillUnload`
4. Just activate the lifercycle. `GgcLifeCycleManager.activate();`


# Can be used standalone

App will load as it was before when used standalone, but when embedded in assessment center, it will run as a component

